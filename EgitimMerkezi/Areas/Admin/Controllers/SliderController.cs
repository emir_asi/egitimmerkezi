﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EgitimMerkezi.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        EgitimMerkeziEntities db = new EgitimMerkeziEntities();
        // GET: Admin/Slider
        public ActionResult Index()
        {
            var model = db.Slider.OrderByDescending(x => x.ID).ToList();
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult AddSlider(Slider model)
        {
            if (ModelState.IsValid)
            {
                db.Slider.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}